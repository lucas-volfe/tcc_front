import React, {useState} from "react";
import NumberFormat from "react-number-format";
import {Grid, TextField} from "@material-ui/core";

const RenderNumberFormatField = ({register, watch, label, name, validators, setValue}) => {
    const [value, setFieldValue] = useState();


    const NumberFormatCustom = (props) => {
        const { inputRef, onChange, ...other } = props;

        return (
            <NumberFormat
                {...other}
                decimalSeparator={','}
                getInputRef={inputRef}
                onValueChange={(values) => {
                    onChange(values.value);
                }}
                isNumericString

            />
        );
    }

    return (
        <TextField
            {...register(name, validators)}
            label={label}
            value={value}
            onChange={v => {
                console.log(v)
                setFieldValue(v)
            }}
            onBlur={event => setValue(name, value)}
            name={name}
            variant="outlined"
            InputProps={{
                inputComponent: NumberFormatCustom,
            }}
            fullWidth
        />
    );
}

export default RenderNumberFormatField;
