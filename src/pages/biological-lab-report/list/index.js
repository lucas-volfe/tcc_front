import * as React from 'react';
import {useCallback, useEffect, useState} from "react";
import TableComponent from "../../../components/table";
import ApiService from "../../../service";

export default function BiologicalLabReportListPage (){
    const [data, setData] = useState([]);
    const columns = [
        {
            name: "grid",
            label: "Grid / Talhão / Propriedade",
            options: {customBodyRender: (value)=> value?.identifier + ' - ' + value?.plot_name}
        },
        {
            name: 'name_lab',
            label: "Nome Laboratório",
        },
        {
            name: 'report_number_lab',
            label: "número de relatório do laboratório",
        },
        {
            name: 'cbm',
            label: "Cbm",
        },
        {
            name: 'beta_glucosidase',
            label: "Beta Glucosidase",
        },
        {
            name: 'ariphosphatase',
            label: "Arifosfatase",
        },
        {
            name: 'acid_phosphatase',
            label: "Fosfatase Ácida",
        },
        {
            name: 'organic_matter',
            label: "Matéria Orgânica",
        },
    ];

    const getData = useCallback(async () => {
        let response = await ApiService.get('biological-lab-report/')
        if (response?.status === 200)
            setData(response?.data)
    }, [])

    useEffect(()=>{
        getData();
    }, [getData]);


    return <TableComponent
        columns={columns}
        data={data}
        title={'Análises Biológicas'}
    />
}
