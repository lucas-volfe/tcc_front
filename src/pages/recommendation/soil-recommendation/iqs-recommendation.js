import React from 'react';
import { Box, Card, CardContent, Grid, Typography } from "@material-ui/core";
import SoilClassBar from "../soil-class-bar";

export default function IqsRecommendation({ data }) {
    return (
        <Card style={{ height: '100%' }}>
            <CardContent>
                <Typography variant={'h5'}>
                    Índice de Qualidade do Solo (IQS)
                </Typography>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <Typography>Carbono da Biomassa (mg de C/kg de solo): {data?.biomass_carbon?.value}</Typography>
                        <SoilClassBar indexClass={data?.biomass_carbon?.soil_class} 
                            chipLabel={data?.biomass_carbon?.soil_class} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>B-glicosidase (μg de p-nitrofenol/g de solo/h): {data?.beta_glucosidase?.value}</Typography>
                        <SoilClassBar indexClass={data?.beta_glucosidase?.soil_class} 
                            chipLabel={data?.beta_glucosidase?.soil_class} />
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>Fosfatase Ácida (μg de p-nitrofenol/g de solo/h): {data?.acid_phosphatase?.value}</Typography>
                        <SoilClassBar indexClass={data?.acid_phosphatase?.soil_class}
                            chipLabel={data?.ariphosphatase?.soil_class} />
                    </Grid>
                    <Grid item xs={6}>
                        <Typography>Arisulfatase (μg de p-nitrofenol/g de solo/h): {data?.ariphosphatase?.value}</Typography>
                        <SoilClassBar indexClass={data?.ariphosphatase?.soil_class}
                            chipLabel={data?.acid_phosphatase?.soil_class} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography>Matéria Organica (%): {data?.organic_matter?.value}</Typography>
                        <SoilClassBar indexClass={data?.organic_matter?.soil_class} 
                            chipLabel={data?.organic_matter?.soil_class} />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography>IQS Antec: {data?.iqs?.value}</Typography>
                        <SoilClassBar indexClass={data?.iqs?.soil_class} 
                            chipLabel={data?.iqs?.soil_class} />
                    </Grid>
                </Grid>

            </CardContent>
        </Card>
    )
}
